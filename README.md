# Finite state automata

## Online example
- Visit online version [http://159.203.37.68/](http://159.203.37.68/)
- (this is a temporary instance just for this purpose)


#### Run on your local environment
- Clone the repository: `git clone git@bitbucket.org:rpaleka/fsa.git`
- Run the following command: `composer install && cp .env.example .env && php artisan key:generate && php artisan optimize`
- To start local laravel instance run `php artisan server`
- Go to [http://127.0.0.1:8000/](http://127.0.0.1:8000/) and you should be welcomed with the following screen 
<p align="center">
<img src="http://159.203.37.68/screen-homepage.png">
</p>

- Run unit tests with `./vendor/bin/phpunit` from your project root folder


#### Possible issues
For error "No application encryption key has been specified." :
- remove `.env`
- run `php artisan key:generate`
- run local server `php artisan serve`


### Code
- Fsa code can be found in`app/FSA`
- test are in `tests/Unit/FSATest.php` file
- frontend controller `app/Http/Controllers/ShowStatesController.php`
