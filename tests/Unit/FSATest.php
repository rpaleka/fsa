<?php

namespace Tests\Unit;

use App\FSA\FiniteStateAutomata;
use PHPUnit\Framework\TestCase;

class FSATest extends TestCase
{

    /**
     * @var FiniteStateAutomata
     */
    private $fsa;

    protected function setUp() : void
    {
        $this->fsa = new FiniteStateAutomata();
    }
    public function testState_0_oncreate()
    {
        $this->assertEquals($this->fsa->state, FiniteStateAutomata::S0);
    }

    public function testState_0_oncall()
    {
        $this->fsa->S0();
        $this->assertEquals($this->fsa->state, FiniteStateAutomata::S0);
    }

    public function testState_1_oncall()
    {
        $this->fsa->S1();
        $this->assertEquals($this->fsa->state, FiniteStateAutomata::S1);
    }

    public function testState_2_oncall()
    {
        $this->fsa->S2();
        $this->assertEquals($this->fsa->state, FiniteStateAutomata::S2);
    }

    public function testResultCaseOneNoError()
    {
        $inputString = '1010';
        $inputArray = str_split($inputString);

        foreach ($inputArray as $input) {
            $this->fsa->transitionWithInput($input);
        }

        $this->assertEquals($this->fsa->result, '(output for state S1 = 1) <---- This is the answer');
    }

    public function testResultCaseTwoNoError()
    {
        $inputString = '110';
        $inputArray = str_split($inputString);

        foreach ($inputArray as $input) {
            $this->fsa->transitionWithInput($input);
        }

        $this->assertEquals($this->fsa->result, '(output for state S0 = 0) <---- This is the answer');
    }

    public function testSetInput()
    {
        $this->fsa->S0();
        $input = 1;
        $this->fsa->setInput($input);
        $this->assertEquals($input, $this->fsa->getInput());
    }

    public function testResultCaseNoStateThrowException()
    {
        $inputString = '3';
        $this->expectExceptionMessage('Note: this will never happen with our above machine as all state can be final state');
        $this->fsa->transitionWithInput($inputString);
    }
}
