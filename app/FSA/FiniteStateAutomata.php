<?php

namespace App\FSA;

use Exception;
use App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FiniteStateAutomata
 * @package App\FSA
 */
class FiniteStateAutomata extends Model
{
    const S0 = 'S0';
    const S1 = 'S1';
    const S2 = 'S2';

    // simplified finite states table for checking allowed states
    private $allowedTransitionsTable = [
        'S00' => 'S0',
        'S01' => 'S1',
        'S10' => 'S2',
        'S11' => 'S0',
        'S20' => 'S1',
        'S21' => 'S2',
    ];

    private $stateOutputs = [
        'S0' => 0,
        'S1' => 1,
        'S2' => 2
    ];
    /**
     * @var object - Current state
     */
    protected $state;

    /**
     * @var integer - Zero or one
     */
    protected $input;

    /**
     * @var string - Output for the frontend
     */
    public $result;

    /**
     * @var string - Logged steps of the FSA states
     */
    public $steps = '';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        // user service container to create default state
        $this->state = app()->makeWith(S0::class, ['context' => $this]);
    }

    public function S0()
    {
        $this->state->S0();
    }

    public function S1()
    {
        $this->state->S1();
    }

    public function S2()
    {
        $this->state->S2();
    }

    public function getStateAttribute()
    {
        return $this->state;
    }

    public function setStateAttribute($value)
    {
        $this->state = $value;
    }

    public function setInput($input)
    {
        $this->input = $input;
    }

    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param $input
     *
     * @throws Exception
     */
    public function transitionWithInput($input)
    {
        $this->setInput($input);
        $currentState = $this->checkTransitionTable();

        $this->steps .= "Current state = " . $state1 = strval($this->getStateAttribute()) . ", ";

        // !important go to next state
        $this->{$currentState}();

        $this->steps .= " Input = " . $this->getInput() . ", ";
        $this->steps .= " result state=  " . strval($this->getStateAttribute()) . "<br>";

        $resultString = '(output for state ' . $currentState . ' = ' . $this->stateOutputs[$currentState] . ') <---- This is the answer';
        $this->result = $resultString;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function checkTransitionTable()
    {
        // current state
        $currentStateTableName = strval($this->getStateAttribute()) . $this->getInput();

        // set future state that will be used as current state in the next step
        if (array_key_exists($currentStateTableName, $this->allowedTransitionsTable)) {
            return $this->allowedTransitionsTable[$currentStateTableName];
        } else {
            throw new Exception('Note: this will never happen with our above machine as all state can be final state');
        }
    }
}
