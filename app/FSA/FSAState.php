<?php
namespace App\FSA;

interface FSAState
{
    public function __construct($context);
    public function S0();
    public function S1();
    public function S2();
    public function __toString();
}
