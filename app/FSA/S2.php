<?php

namespace App\FSA;

class S2 implements FSAState
{
    protected $context;

    public function __construct($context)
    {
        $this->context = $context;
    }

    public function S0()
    {
        $this->context->state = new S0($this->context);
    }

    public function S1()
    {
        $this->context->state = new S1($this->context);
    }

    public function S2()
    {
        $this->context->state = new S2($this->context);
        //throw new \Exception('Note: this will never happen with our above machine as all state can be final state');
    }

    public function __toString()
    {
        return FiniteStateAutomata::S2;
    }
}
