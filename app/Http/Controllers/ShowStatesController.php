<?php

namespace App\Http\Controllers;

use App\FSA\FiniteStateAutomata;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

class ShowStatesController extends BaseController
{
    /**
     * Simple controller to parse the string and init finite state machine
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        // set to default state if nothing is provided via url variables
        if (isset($request->input)) {
            $inputString = $request->get('input');
        } else {
            $inputString = '110';
        }

        // split string to individual letters
        $inputArray = str_split($inputString);
        $fsa = new FiniteStateAutomata();

        foreach ($inputArray as $input) {
            $fsa->transitionWithInput($input);
        }

        return view('fsa',
            [
                'result' => $fsa->result,
                'steps' => $fsa->steps,
                'inputString' => $inputString,
            ]
        );
    }
}
